package io.spring;

import io.spring.api.ArticleApi;
import io.spring.application.ArticleQueryService;
import io.spring.application.data.ArticleData;
import io.spring.core.article.Article;
import io.spring.core.user.User;
import io.spring.infrastructure.mybatis.readservice.ArticleFavoritesReadService;
import io.spring.infrastructure.mybatis.readservice.ArticleReadService;
import io.spring.infrastructure.mybatis.readservice.UserRelationshipQueryService;
import org.joda.time.DateTime;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import java.util.HashMap;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class IntegracionTest {

    @Mock
    private ArticleReadService articleReadSErvice;
    @Mock
    private ArticleFavoritesReadService articleFavoritesReadServiceReadSErvice;
    @Mock
    private UserRelationshipQueryService userRelationshipQueryService;



    @Test
    public void articleTest(){

            String email = "john@jacob.com";
            String username = "johnjacob";
            String defaultAvatar = "https://static.productionready.io/images/smiley-cyrus.jpg";

            User user = new User(email, username, "123", "", defaultAvatar);
            String slug = "test-new-article";
            DateTime time = new DateTime();
            Article article = new Article("Test New Article", "Desc", "Body", new String[]{"java", "spring", "jpg"}, user.getId(), time);
            ArticleData articleData = TestHelper.getArticleDataFromArticleAndUser(article, user);


            Mockito.when(articleReadSErvice.findBySlug(slug)).thenReturn(articleData);
            Mockito.when(articleFavoritesReadServiceReadSErvice.isUserFavorite(user.getId(),article.getId())).thenReturn(false);
        Mockito.when(articleFavoritesReadServiceReadSErvice.articleFavoriteCount(article.getId())).thenReturn(1);

            ArticleQueryService articleQueryService = new ArticleQueryService(articleReadSErvice, userRelationshipQueryService, articleFavoritesReadServiceReadSErvice);

            ArticleApi sut = new ArticleApi(articleQueryService,null);

        Map<String, Object> envoltura = new HashMap<String, Object>() {{
            put("article", articleData); }};



        ResponseEntity resultadoObtenido = sut.article(slug,user);
        Object a = resultadoObtenido.getBody();
        assertEquals(envoltura,a);








        }


    }

