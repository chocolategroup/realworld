package io.spring;

import static org.junit.Assert.*;
import org.junit.Test;

public class ArticleSpec {
    @Test
    public void probandoUpdateToSlugEspacios(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String tituloPrueba = "hola mundo";
        String resultadoEsperado = "hola-mundo";
        //Ejecución
        articulo.update(tituloPrueba, "a","a");
        String resultadoObtenido = articulo.getSlug();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);
    }
    @Test
    public void probandoUpdateToSlugMayusculas(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String tituloPrueba = "hoLa Mundo";
        String resultadoEsperado = "hola-mundo";
        //Ejecución
        articulo.update(tituloPrueba, "a","a");
        String resultadoObtenido = articulo.getSlug();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);}
    @Test
    public void probandoUpdateToSlugNumero(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String tituloPrueba = "hola mundo 3";
        String resultadoEsperado = "hola-mundo-3";
        //Ejecución
        articulo.update(tituloPrueba, "a","a");
        String resultadoObtenido = articulo.getSlug();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);}
    @Test
    public void probandoUpdateToSlugCaracteresEspeciales(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String tituloPrueba = "hola?mundo";
        String resultadoEsperado = "hola-mundo";
        //Ejecución
        articulo.update(tituloPrueba, "a","a");
        String resultadoObtenido = articulo.getSlug();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);}
    @Test
    public void probandoUpdateTituloVacio(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String tituloPrueba = "";
        String resultadoEsperado = "a";
        //Ejecución
        articulo.update(tituloPrueba, "a","a");
        String resultadoObtenido = articulo.getTitle();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);}
    @Test
    public void probandoUpdateDescripcionVacia(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String decripcionPrueba = "";
        String resultadoEsperado = "a";
        //Ejecución
        articulo.update("a",decripcionPrueba ,"a");
        String resultadoObtenido = articulo.getTitle();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);}
        @Test
        public void probandoUpdateBodyVacio(){
        String [] a= new String[1];
        //SUT
        io.spring.core.article.Article articulo = new io.spring.core.article.Article("a", "a", "a", a, "a");

        //Datos de prueba
        String bodyPrueba = "";
        String resultadoEsperado = "a";
        //Ejecución
        articulo.update("a","a",bodyPrueba);
        String resultadoObtenido = articulo.getTitle();
        //Aserción
        assertEquals(resultadoEsperado,resultadoObtenido);}





    }
