package io.spring;

import io.spring.api.ArticleApi;
import io.spring.application.ArticleQueryService;
import io.spring.application.data.ArticleData;
import io.spring.core.article.Article;
import io.spring.core.article.ArticleRepository;
import io.spring.core.user.User;
import io.spring.core.user.UserRepository;
import io.spring.infrastructure.mybatis.mapper.ArticleMapper;
import io.spring.infrastructure.mybatis.readservice.ArticleFavoritesReadService;
import io.spring.infrastructure.mybatis.readservice.ArticleReadService;
import io.spring.infrastructure.mybatis.readservice.UserRelationshipQueryService;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureTestDatabase
public class FuncionalTest {
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private ArticleReadService articleReadSErvice;
    @Autowired
    private ArticleFavoritesReadService articleFavoritesReadServiceReadSErvice;
    @Autowired
    private UserRelationshipQueryService userRelationshipQueryService;

    @Test
    public void articleTestAltoNivel(){
        //Guarda los datos de prueba en la db
        User user = new User("a@a.com", "yo", "yo", "a", "smily");
        userRepository.save(user);
        Article article = new Article("prueba", "a", "a", new String[]{"a", "b"}, user.getId());
        articleRepository.save(article);


        ArticleQueryService articleQueryService = new ArticleQueryService(articleReadSErvice, userRelationshipQueryService, articleFavoritesReadServiceReadSErvice);

        ArticleApi sut = new ArticleApi(articleQueryService,articleRepository);
        ResponseEntity resultadoObtenido = sut.article(article.getSlug(),user);
        assertTrue(resultadoObtenido.getStatusCodeValue()==200);










    }


}
